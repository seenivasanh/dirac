:orphan:
 

star(NQCC)

This section gives directives for the calculation of Nuclear Quadrupole
Coupling Constants (NQCC). By default they are given in the EFG principal axis system.


**Advanced options**

keyword(CARTESIAN)

Activate the calculation of NQCC in the cartesian axis system.
If symmetry detection is activated, the molecule will be placed
into the principal axis system of the moment of inertia tensor 
and centered at center of mass so that he NQCC will be given in that axis system, 
following the same axis convention used in Refs. :cite:`Bauder1997,Soulard2006,Gaul2020`.

*Default:* Use the EFG principal axis system.


