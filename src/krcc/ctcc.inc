      COMMON/KRCC_CTCC/
! max something
     &             MX_ST_TSOSO_MX,MX_ST_TSOSO_BLK_MX,
     &             LEN_T_VEC,LEN_T_VEC_MX,LEN_T_VEC_CC,MX_SBSTR,NALLINT,
! Not really used but kept
! look at IDIM_TCC_KRCC
     &             N1ELINT,N2ELINT,ILTTWO,NHOL_SPC,
! Dimensions for H 
     &             NSPOBEX_TP,
     &             NH22,NH21,NH12,NH20,
     &             NH11,NH02,NH10,NH01,NH00,
! T
     &             NOBEX_TP,NSPOBEX_TPE,NSPOBEX_TP_CC,
! M
     &             NINTER10,NINTER01,NINTER11,
     &             NINTER02,NINTER20,NINTER12,
! For help to M
     &             LENA_KRCC,LENB_KRCC,
! Contraction mappings
     &             LEN_M01_VEC_CC,LEN_M10_VEC_CC,
     &             LEN_M11_VEC_CC,LEN_M02_VEC_CC,LEN_M12_VEC_CC,
! Other
     &             NAOBEX_TP,NBOBEX_TP
